Quickstart Kolide
=================

### QuickStart
```
bash gen-cert.sh
docker-compose up -d
```

Access Kolide at `localhost:8412`

Build macOS package using: https://github.com/kolide/launcher/blob/master/docs/package-builder.md

osquery logs can be found in `kolide/osquery_result` and `kolide/osquery_status`

### Mailhog

Use SMTP mailhog:1025 to see all mails.
Access Mailhog (all sent mails) at `localhost:8025`
