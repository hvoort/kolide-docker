#!/bin/bash

CN=kolide
openssl genrsa -out kolide/server.key 2048
openssl rsa -in kolide/server.key -out kolide/server.key
openssl req -sha256 -new -key kolide/server.key -out kolide/server.csr -subj "/CN=${CN}"
openssl x509 -req -sha256 -days 365 -in kolide/server.csr -signkey kolide/server.key -out kolide/server.crt
rm kolide/server.csr
